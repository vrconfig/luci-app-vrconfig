```
 \ \     /    _ \     ___|                    _|  _)         
  \ \   /    |   |   |        _ \    __ \    |     |    _` | 
   \ \ /     __ <    |       (   |   |   |   __|   |   (   | 
    \_/     _| \_\  \____|  \___/   _|  _|  _|    _|  \__, | 
                                                      |___/  

```




Visual router config for LuCI

### How to install VRConfig ?

1. Add `src-git vrconfig git@gitlab.com:vrconfig/luci-app-vrconfig.git` to your feeds.conf
2. Run `./scripts/feeds update vrconfig`
3. Run `./scripts/feeds install luci-app-vrconfig`
4. Run `make menuconfig` and select luci-app-vrconfig in LuCi/Applications
5. Run `make package/feeds/vrconfig/luci-app-vrconfig/compile` to create an ipk package
