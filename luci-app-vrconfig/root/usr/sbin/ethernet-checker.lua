#!/usr/bin/lua

--[[

Copyright (C) 2018 Tobias Ilte <tobias.ilte@campus.tu-berlin.de>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

--]]


require "uci"
require "ubus"
require "uloop"
require "nixio"
log = require "nixio"

file = require "nixio.fs"

--file = require "nixio"

--open sys-logging
--log.openlog("VRConfig", "ndelay", "cons", "nowait");

local interval = 5




ubus = ubus.connect()
if not ubus then
  error("Failed to connect to ubusd")
end
ubus:call("network", "reload", {})

--print(table.getn(hosts))

--log.syslog("info","VRConfig ftw!!")

uloop.init()


function do_ethernet_check()
 
  --result = os.execute("curl -s -m "..timeout.." --max-redirs 0 --head "..host.." > /dev/null")
  --result = os.execute("cat /sys/class/net/eth0/carrier")
  eth0 = file.readfile("/sys/class/net/eth0/carrier")
  eth1 = file.readfile("/sys/class/net/eth1/carrier")
  print("eth0:"..eth0)
  
  result2 = '{"eth0":'..eth0..',"eth1":'..eth1..'}'

  file.writefile("/www/luci-static/resources/vrconfig/ethernet-status.json",result2)
  	
  timer:set(interval * 1000)	
  	   
end



timer = uloop.timer(do_ethernet_check)
timer:set(interval * 1000)

uloop.run()

